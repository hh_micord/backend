from django.contrib.auth import get_user_model

from rest_framework import viewsets
from rest_framework.permissions import AllowAny

from book.models import Book, Author
from book.api.serializers import BookSerializer, AuthorSerializer

User = get_user_model()


class BooksViewSet(viewsets.ModelViewSet):
    """ Получаем список всех книг
    """

    queryset = Book.objects.all()
    permission_classes = [AllowAny]
    allowed_methods = ["get"]
    serializer_class = BookSerializer


class AuthorViewSet(viewsets.ModelViewSet):
    """ Получаем список всех авторов
    """

    queryset = Author.objects.all()
    permission_classes = [AllowAny]
    allowed_methods = ["get"]
    serializer_class = AuthorSerializer
