from django.contrib.auth import get_user_model
from rest_framework import serializers

from book.models import Book, Author

User = get_user_model()


class BookSerializer(serializers.ModelSerializer):
    class Meta:
        model = Book
        fields = (
            "id",
            "name",
            "price",
            "author_id",
        )


class AuthorSerializer(serializers.ModelSerializer):
    book = BookSerializer(many=True, read_only=True)
    book_count = serializers.SerializerMethodField()

    class Meta:
        model = Author
        fields = (
            "id",
            "fio",
            "book",
            "book_count",
        )

    def get_book_count(self, obj):
        return obj.book.count()
