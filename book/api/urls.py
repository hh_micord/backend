from django.conf.urls import url
from django.urls import include

from rest_framework import routers

from book.api.views import BooksViewSet, AuthorViewSet

app_name = "book"

router = routers.DefaultRouter()
router.register(r"books", BooksViewSet)
router.register(r"authors", AuthorViewSet)

urlpatterns = [
    url("^", include(router.urls)),
]
