from django.db import models
from django.core.validators import MinValueValidator


class Author(models.Model):
    fio = models.CharField("ФИО", max_length=255)

    def __str__(self):
        return self.fio

    class Meta:
        app_label = "book"
        verbose_name = "Автор"
        verbose_name_plural = "Авторы"
        ordering = ["fio"]


class Book(models.Model):
    name = models.CharField("Название", max_length=100)
    price = models.IntegerField("Цена", validators=[MinValueValidator(1)])
    author_id = models.ForeignKey(
        Author,
        null=False,
        blank=True,
        related_name="book",
        verbose_name="Автор",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name

    class Meta:
        app_label = "book"
        verbose_name = "Книга"
        verbose_name_plural = "Книги"
        ordering = ["name"]
