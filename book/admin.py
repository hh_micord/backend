from django.contrib import admin

from .models import Author, Book


@admin.register(Author)
class AuthorAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "fio",
    )
    list_filter = ("fio",)


@admin.register(Book)
class BookAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "name",
        "price",
        "author_id",
    )
    list_filter = ("price", "author_id")
    list_editable = ("price",)
