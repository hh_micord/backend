from django.core.mail import mail_admins


def send_message(header, message):
    mail_admins(header, message, fail_silently=False)
