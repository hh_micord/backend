from django.contrib import admin

from .models import Order


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "phone_number",
        "comment",
        "date",
        "user_id",
        "book_id",
    )
    list_filter = ("date", "phone_number", "user_id", "book_id")
