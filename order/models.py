from django.db import models

from phonenumber_field.modelfields import PhoneNumberField

from book.models import Book
from user.models import Profile


class Order(models.Model):
    phone_number = PhoneNumberField("Телефон покупателя")
    comment = models.TextField("Комментарий", max_length=2000, blank=True)
    date = models.DateField("Дата заявки", auto_now_add=True)

    user_id = models.ForeignKey(
        Profile,
        null=False,
        blank=True,
        related_name="order",
        verbose_name="Пользователь",
        on_delete=models.CASCADE,
    )
    book_id = models.ForeignKey(
        Book,
        null=False,
        blank=True,
        related_name="order",
        verbose_name="Книга",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return f"{self.date} {self.user_id}"

    class Meta:
        app_label = "order"
        verbose_name = "Заявка"
        verbose_name_plural = "Заявки"
        ordering = [
            "date",
            "user_id",
            "phone_number",
        ]
