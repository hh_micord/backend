from rest_framework import serializers

import phonenumbers

from user.models import Profile
from book.models import Book
from order.models import Order


def validate_phone_serializer(serializer, value):
    """Валидируем номер и приводим к типу PhoneNumber из phonenumbers"""
    try:
        phone = phonenumbers.parse(value)
        if not phonenumbers.is_possible_number(
            phone
        ) or not phonenumbers.is_valid_number(phone):
            raise serializers.ValidationError(
                detail={"phone_number": "Неправильный формат номера телефона."}
            )
    except phonenumbers.NumberParseException:
        raise serializers.ValidationError(
            detail={"phone_number": "Неправильный формат номера телефона."}
        )

    return phone


def get_phone_number_field(validators=None):
    return serializers.CharField(
        min_length=12,
        max_length=18,
        required=True,
        validators=validators,
        error_messages={
            "invalid": "Неправильный формат номера телефона.",
            "blank": "Номер не может быть пустым.",
            "required": "Номер должен быть указан.",
            "max_length": "Номер должен содержать менее {max_length} символов.",
            "min_length": "Номер должен содержать как минимум {min_length} символов.",
        },
    )


class OrderSerializer(serializers.ModelSerializer):
    phone_number = get_phone_number_field()
    user_id = serializers.IntegerField()
    book_id = serializers.IntegerField()
    comment = serializers.CharField(required=False, allow_blank=True)

    def create(self, validated_data: dict) -> Order:
        user = Profile.objects.get(id=validated_data["user_id"])
        book = Book.objects.get(id=validated_data["book_id"])
        order = Order(
            user_id=user,
            comment=validated_data["comment"],
            book_id=book,
            phone_number=validated_data["phone_number"],
        )
        order.save()
        return order

    def validate(self, attrs):
        if not attrs["user_id"]:
            raise serializers.ValidationError(
                detail={"user_id": "ID пользователя должен быть указан."}
            )
        if not Profile.objects.get(id=attrs["user_id"]):
            raise serializers.ValidationError(
                detail={"user_id": "Неправильный ID пользователя."}
            )
        if not attrs["book_id"]:
            raise serializers.ValidationError(
                detail={"book_id": "ID книги должен быть указан."}
            )
        if not Book.objects.get(id=attrs["book_id"]):
            raise serializers.ValidationError(
                detail={"book_id": "Неправильный ID книги."}
            )
        if not attrs["phone_number"]:
            raise serializers.ValidationError(
                detail={"phone_number": "Номер должен быть указан."}
            )

        validate_phone_serializer(serializers, attrs["phone_number"])

        return attrs

    class Meta:
        model = Order
        fields = [
            "comment",
            "phone_number",
            "user_id",
            "book_id",
        ]
