from rest_framework import viewsets
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from order.models import Order
from order.api.serializers import OrderSerializer
from order.utils import send_message


class OrderViewSet(viewsets.ModelViewSet):
    """ Создаем заявку на покупку
    """

    queryset = Order.objects.all()
    permission_classes = [AllowAny]
    allowed_methods = ["post"]
    serializer_class = OrderSerializer

    def create(self, request, *args, **kwargs):
        data = request.data

        serializer = OrderSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        send_message(
            "Тестовое",
            f"""Была создана заявка на покупку книги:
    Номер телефона: {serializer.validated_data['phone_number']}
    ID пользователя: {serializer.validated_data['user_id']}
    ID книги: {serializer.validated_data['book_id']}
    Комментарий: {serializer.validated_data['comment']}""",
        )

        return Response(status=200, data={"status": "Success"})
