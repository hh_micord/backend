from django.conf.urls import url
from django.urls import include

from rest_framework import routers

from order.api.views import OrderViewSet

app_name = "order"

router = routers.DefaultRouter()
router.register(r"order", OrderViewSet)

urlpatterns = [
    url("^", include(router.urls)),
]
