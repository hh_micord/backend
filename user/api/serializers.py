from django.contrib.auth import get_user_model, authenticate
from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from user.models import Profile

User = get_user_model()


def get_email_field(validators=None):
    return serializers.EmailField(
        required=True,
        validators=validators,
        error_messages={
            "invalid": "Указан некорректный e-mail.",
            "blank": "Email не может быть пустым.",
            "required": "Значение должно быть указано.",
        },
    )


def get_password_field():
    return serializers.CharField(
        min_length=6,
        max_length=100,
        write_only=True,
        required=True,
        trim_whitespace=False,
        error_messages={
            "invalid": "Значение не корректно.",
            "blank": "Пароль не может быть пустым.",
            "required": "Значение должно быть указано.",
            "max_length": "Пароль должен содержать менее {max_length} символов.",
            "min_length": "Пароль должен содержать как минимум {min_length} символов.",
        },
    )


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            "id",
            "username",
            "email",
        ]


class LoginedProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = [
            "id",
        ]


class RegisterProfileSerializer(serializers.ModelSerializer):
    user = serializers.IntegerField()

    def create(self, validated_data: dict) -> Profile:
        user = User.objects.get(id=validated_data["user"])
        profile = Profile(user=user)
        profile.save()
        return profile

    class Meta:
        model = Profile
        fields = [
            "user",
        ]


class RegisterSerializer(serializers.ModelSerializer):
    email = get_email_field(
        validators=[
            UniqueValidator(
                queryset=User.objects.all(), message="Введенный email уже используется."
            )
        ]
    )

    password = get_password_field()
    passwordrep = get_password_field()

    def create(self, validated_data: dict) -> User:
        user = User(
            email=validated_data["email"],
            username=validated_data["email"],
            is_active=True,
        )
        user.set_password(validated_data["password"])
        user.save()
        return user

    def validate(self, attrs):
        if attrs["password"] != attrs["passwordrep"]:
            raise serializers.ValidationError(
                detail={
                    "password": "Введенные пароли не совпадают",
                    "passwordrep": "Введенные пароли не совпадают",
                }
            )

        return attrs

    class Meta:
        model = User
        fields = (
            "id",
            "email",
            "password",
            "passwordrep",
        )


class LoginSerializer(serializers.Serializer):
    email = get_email_field()
    password = get_password_field()

    def validate(self, attrs):
        user = User.objects.filter(email=attrs.get("email")).first()
        if not user:
            raise serializers.ValidationError(
                detail={
                    "email": True,
                    "password": True,
                    "common": "Введенная пара email-пароль не найдена!",
                }
            )

        username = user.username
        password = attrs["password"]
        user = authenticate(
            request=self.context.get("request"), username=username, password=password
        )

        # The authenticate call simply returns None for is_active=False
        # users. (Assuming the default ModelBackend authentication
        # backend.)
        if not user:
            raise serializers.ValidationError(
                detail={
                    "email": True,
                    "password": True,
                    "common": "Введенная пара email-пароль не найдена!",
                }
            )

        attrs["user"] = user
        return attrs
