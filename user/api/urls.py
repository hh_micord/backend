from django.conf.urls import url
from knox.views import LogoutView

from user.api.views import RegistrationView, LoginView

app_name = "user"
urlpatterns = [
    # auth API (DFR knox + my own)
    # https://github.com/James1345/django-rest-knox
    url(r"register/", RegistrationView.as_view()),
    url(r"login/", LoginView.as_view()),
    url(r"logout/", LogoutView.as_view(), name="knox_logout"),
]
