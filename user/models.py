from django.db import models
from django.contrib.auth import get_user_model

User_model = get_user_model()


class Profile(models.Model):
    user = models.OneToOneField(
        User_model,
        verbose_name="Пользователь",
        related_name="profile",
        on_delete=models.CASCADE,
        db_index=True,
    )

    def __str__(self):
        return self.user.username

    class Meta:
        app_label = "user"
        verbose_name = "Пользователь"
        verbose_name_plural = "Пользователи"
