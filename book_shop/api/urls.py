from django.urls import path, include

urlpatterns = [
    # auth API
    path("books/", include("book.api.urls", namespace="book")),
    path("order/", include("order.api.urls", namespace="order")),
    path("auth/", include("user.api.urls", namespace="user")),
]
